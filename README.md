# Inference and evaluation of enhancer‐mediated gene regulatory networks with GRaNIE and GRaNPA, EMBL-EBI Course - Systems biology: from large datasets to biological insight (2024)

[![Lifecycle: experimental](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://www.tidyverse.org/lifecycle/#experimental) [![minimal R version](https://img.shields.io/badge/R%3E-4.3.3-6666ff.svg)](https://cran.r-project.org/)

## Course website

<https://www.ebi.ac.uk/training/events/systems-biology-large-datasets-biological-insight-2>

## Talks

-   [x] [Judith Zaugg](presentations/judith.pdf)
-   [x] [Christian Arnold](presentations/christian.pdf)
-   [x] [Aryan Kamal](presentations/aryan.pdf)


## Data and Results

-   GRaNIE input files
    -   [HOCOMOCO v12 TFBS predictions for the top 100 TFs from the eGRN](https://s3.embl.de/zaugg-web/GRaNIE/exampleData/NEUrIPS2021/traj2_res10/PWMScan_HOCOMOCOv12_filtered.tar.gz)
    -   [RNA](https://s3.embl.de/zaugg-web/GRaNIE/exampleData/NEUrIPS2021/traj2_res10/rna.pseudobulkFromClusters_res10_mean.tsv.gz), [ATAC](https://s3.embl.de/zaugg-web/GRaNIE/exampleData/NEUrIPS2021/traj2_res10/atac.pseudobulkFromClusters_res10_mean.tsv.gz), and [metadata](https://s3.embl.de/zaugg-web/GRaNIE/exampleData/NEUrIPS2021/traj2_res10/metadata_res10_mean.tsv.gz) files for the erythroid trajectory data
-   GRaNPA input files
    -   [GRaNIE output object for the erythroid trajectory data](https://s3.embl.de/zaugg-web/GRaNIE/exampleData/NEUrIPS2021/traj2_res10/output/GRN.qs)
    -   [GRaNIE output object for the lymphoid trajectory data](https://s3.embl.de/zaugg-web/GRaNIE/exampleData/NEUrIPS2021/traj1_res5/output/GRN.qs)
    -   [DE genes](https://s3.embl.de/zaugg-web//GRaNPA/NEUrIPS2021/DE/final_lists.qs)

## Vignettes

-   [x] [GRaNIE preparation](vignettes/GRaNIE_prepare.html)
-   [x] [GRaNIE workflow](vignettes/GRaNIE_workflow.html)
-   [x] [GRaNPA workflow](vignettes/GRaNPA_workflow.pdf)
